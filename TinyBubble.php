<?php
namespace TinyBubble;

require_once 'config.php';

use PDO;

class TinyBubble {
	public static function pop() {
        $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        $parsed_url = parse_url($url);
        $path = ltrim($parsed_url['path'], '/');
        parse_str(isset($parsed_url['query']) ? $parsed_url['query'] : '', $query);
        $pathParts = explode('/', $path);
        $controller = isset($pathParts[0]) && !empty($pathParts[0]) ? $pathParts[0] : 'home';
		$action = isset($pathParts[1]) && !empty($pathParts[1]) ? $pathParts[1] : 'Index';
        $parameter = isset($pathParts[2]) && is_numeric($pathParts[2]) ? $pathParts[2] : null;
        $controller = __NAMESPACE__ . '\\Controllers\\' . $controller . 'Controller';
		$action = 'action' . ucfirst($action);
        if (!class_exists($controller)) {
			header('Location: /error404');
			die();
        }
        $objController = new $controller;
        if (!method_exists($objController, $action)) {
            header('Location: /error404');
			die();
        }
        $objController->$action($parameter, $query);
	}
}

class View {
    public static function render($path, $data = array(), $layout = true) {
        $fullPath = VIEWS_DIR . $path . '.php';
        if (!file_exists($fullPath)) {
            //throw new \ErrorException('View cannot be found');
            header('Content-type: application/json; charset=utf-8');
            die(\json_encode($data));
        } else if ($layout === true) {
            include(VIEWS_DIR . 'Layout/header.php');
            include($fullPath);
            include(VIEWS_DIR . 'Layout/footer.php');
        } else {
            include($fullPath);
        }
        /*if (!empty($data)) {
            foreach ($data as $key => $value) {
                $$key = $value;
            }
        }*/
    }
}

class DB {
    public static $dsn = DB_TYPE . ':host=' . DB_HOSTNAME . ';dbname=' . DB_DATABASE . (DB_TYPE === 'mysql' ? ';charset=utf8mb4' : '');
	public static $user = DB_USERNAME;
	public static $pass = DB_PASSWORD;
	public static $dbh = null;
	public static $sth = null;
	public static $query = '';

	public static function getDbh() {
		if (!self::$dbh) {
			try {
				self::$dbh = new PDO(
					self::$dsn, 
					self::$user, 
					self::$pass
				);
				self::$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			} catch (PDOException $e) {
				die('Error connecting to database: ' . $e->getMessage());
			}
		}
		return self::$dbh; 
	}

	public static function add($query, $param = array()) {
		self::$sth = self::getDbh()->prepare($query);
		return (self::$sth->execute((array) $param)) ? self::getDbh()->lastInsertId() : 0;
	}

	public static function set($query, $param = array()) {
		self::$sth = self::getDbh()->prepare($query);
		return self::$sth->execute((array) $param);
	}
	
	public static function getRow($query, $param = array()) {
		self::$sth = self::getDbh()->prepare($query);
		self::$sth->execute((array) $param);
		return self::$sth->fetch(PDO::FETCH_ASSOC);		
	}
	
	public static function getAll($query, $param = array()) {
		self::$sth = self::getDbh()->prepare($query);
		self::$sth->execute((array) $param);
		return self::$sth->fetchAll(PDO::FETCH_ASSOC);	
	}
	
	public static function getValue($query, $param = array(), $default = null) {
		$result = self::getRow($query, $param);
		if (!empty($result)) {
			$result = array_shift($result);
		}
		return (empty($result)) ? $default : $result;	
	}
	
	public static function getColumn($query, $param = array()) {
		self::$sth = self::getDbh()->prepare($query);
		self::$sth->execute((array) $param);
		return self::$sth->fetchAll(PDO::FETCH_COLUMN);	
	}
}