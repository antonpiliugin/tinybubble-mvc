<form class="form-default" action="/signup/create" method="post">
<h1 class="h3 mb-3 font-weight-normal">Sign Up</h1>
<?php
if (isset($data['errors']) && !empty($data['errors'])) { ?>
<div class="alert alert-danger" role="alert">
<?php foreach($data['errors'] as $error) {
    echo $error . '<br>';
} ?>
</div>
<?php } else if (isset($data['success']) && $data['success'] === true) { ?>
<div class="alert alert-success" role="alert">
You have been successfully registered! Click <a href="/signin">here</a> to sign-in.
</div>
<?php } ?>
<label for="inputUsername" class="sr-only">Username</label>
<input type="text" name="username" id="inputUsername" class="form-control my-2" placeholder="Username" required autofocus>
<label for="inputEmail" class="sr-only">E-mail address</label>
<input type="email" name="email" id="inputEmail" class="form-control my-2" placeholder="E-mail address" required>
<label for="inputPassword" class="sr-only">Password</label>
<input type="password" name="password" id="inputPassword" class="form-control my-2" placeholder="Password" required>
<label for="confirmPassword" class="sr-only">Confirm password</label>
<input type="password" name="password2" id="confirmPassword" class="form-control my-2" placeholder="Confirm password" required>
<button class="btn btn-lg btn-primary btn-block" type="submit">Sign Up</button>
</form>