<main role="main" class="container-fluid pl-md-5">
<div>
<h1>Task managment system</h1>
<p class="lead">Based on TinyBubble MVC: simple, fast and easy to use!</p>
</div>
<div class="mt-4">
<a class="btn btn-outline-primary" href="/signup">Sign up</a>
<a class="btn btn-outline-secondary ml-1" href="/signin">Sign-in</a>
</div>
</main>