<main class="container-fluid w-100 h-100">
<div class="row">
<?php if (empty($data['tasks'])) { ?>
<div class="jumbotron jumbotron-fluid mx-auto bg-white w-100">
<div class="container">
<h1 class="display-4">No tasks =(</h1>
<p class="lead">Create one to be the first!</p>
<a class="btn btn-primary btn-lg" href="/tasks/create" role="button">Create a new task</a>
</div>
</div>
<?php } else { ?>
<div class="p-3 w-100">
<?php if (!empty($data['message'])) { ?>
<div class="alert alert-success" role="alert">
<?php echo $data['message']; ?>
</div>
<?php } ?>
<div class="btn-group">
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Name
  </button>
  <div class="dropdown-menu">
    <a class="dropdown-item" href="/tasks?page=<?php echo $data['currentPage'] . '&orderBy=username&orderDirection=asc'; ?>">Ascending</a>
    <a class="dropdown-item" href="/tasks?page=<?php echo $data['currentPage'] . '&orderBy=username&orderDirection=desc'; ?>">Descending</a>
  </div>
</div>

<div class="btn-group">
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    E-mail
  </button>
  <div class="dropdown-menu">
    <a class="dropdown-item" href="/tasks?page=<?php echo $data['currentPage'] . '&orderBy=email&orderDirection=asc'; ?>">Ascending</a>
    <a class="dropdown-item" href="/tasks?page=<?php echo $data['currentPage'] . '&orderBy=email&orderDirection=desc'; ?>">Descending</a>
  </div>
</div>

<div class="btn-group">
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Status
  </button>
  <div class="dropdown-menu">
    <a class="dropdown-item" href="/tasks?page=<?php echo $data['currentPage'] . '&orderBy=status&orderDirection=asc'; ?>">Ascending</a>
    <a class="dropdown-item" href="/tasks?page=<?php echo $data['currentPage'] . '&orderBy=status&orderDirection=desc'; ?>">Descending</a>
  </div>
</div>

</div>
<div class="list-group px-3 w-100 h-100">
<?php foreach($data['tasks'] as $task) { ?>
<a href="/tasks/show/<?php echo $task['task_id']; ?>" class="list-group-item list-group-item-action">
<div class="d-flex w-100 justify-content-between">
<h5 class="mb-1"><?php echo $task['title']; echo (int)$task['status'] ? ' <span class="badge badge-success">Completed</span>' : ''; echo (int)$task['admin_edit'] ? '<span class="mx-1 badge badge-warning">Admin edit</span>' : '' ?></h5>
<small><?php echo $task['created_at']; ?></small>
</div>
<p class="mb-1"><?php echo nl2br(htmlspecialchars($task['description'])); ?></p>
<small><?php echo $task['email']; ?></small>
</a>
<?php } ?>
</div>
<div class="p-3 w-100">
<a class="btn btn-primary btn-lg" href="/tasks/create" role="button">Create a new task</a>
</div>
<?php if ($data['totalPages'] > 1) { ?>
<nav aria-label="Page navigation example" class="w-100">
<ul class="pagination justify-content-center">
<li class="page-item<?php echo $data['currentPage'] === 1 ? ' disabled' : ''; ?>">
<a class="page-link" href="<?php echo $data['currentPage'] === 1 ? '#" tabindex="-1" aria-disabled="true"' : '/tasks?page=' . ($data['currentPage'] - 1) . $data['orderQuery'] . '"'; ?>>Previous</a>
</li>
<?php for($i =1; $i <= $data['totalPages']; $i++) { ?>
<li class="page-item<?php echo $data['currentPage'] === $i ? ' active" aria-current="page"' : ''; ?>"><a class="page-link" href="/tasks?page=<?php echo $i . $data['orderQuery']; ?>"><?php echo $i; ?></a></li>
<?php } ?>
<li class="page-item<?php echo $data['currentPage'] === $data['totalPages'] ? ' disabled' : ''; ?>">
<a class="page-link" href="<?php echo $data['currentPage'] === $data['totalPages'] ? '#" tabindex="-1" aria-disabled="true"' : '/tasks?page=' . ($data['currentPage'] + 1) . $data['orderQuery'] . '"'; ?>>Next</a>
</li>
</ul>
</nav>
<?php }} ?>
</main>