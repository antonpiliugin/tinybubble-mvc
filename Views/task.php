<form class="form-default" action="/tasks/<?php echo isset($data['create']) ? 'create' : 'edit/' . $data['task_id']; ?>" method="post">
<h1 class="h3 mb-3 font-weight-normal"><?php echo isset($data['create']) ? 'Create a new task' : 'Edit task'; ?></h1>
<?php
$isNew = isset($data['create']);
if (isset($data['errors']) && !empty($data['errors'])) { ?>
<div class="alert alert-danger" role="alert">
<?php foreach($data['errors'] as $error) {
    echo $error . '<br>';
} ?>
</div>
<?php } ?>
<label for="title" class="sr-only">Title</label>
<input type="text" <?php echo $isNew ? '' : ' value="' . $data['title'] . '"'; ?>name="title" id="taskTitle" class="form-control my-2" placeholder="Task title" required autofocus>
<label for="description" class="sr-only">Description</label>
<textarea name="description" id="description" class="form-control my-2" placeholder="Description" rows="5" required><?php echo $isNew ? '' : $data['description']; ?></textarea>
<label for="status" class="sr-only">Status</label>
<?php if (!$isNew) {
    $status = (int)$data['status'];
?>
<select name="status" class="form-control my-2" id="status">
<option value="0"<?php echo $status === 0 ? ' selected' : ''; ?>>In process</option>
<option value="1"<?php echo $status === 1 ? ' selected' : ''; ?>>Completed</option>
</select>
<?php } ?>
<button class="btn btn-lg btn-primary btn-block" type="submit"><?php echo isset($data['create']) ? 'Create a new task' : 'Edit task'; ?></button>
</form>