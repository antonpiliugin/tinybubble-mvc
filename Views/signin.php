<form class="form-default" action="/signin/auth" method="post">
<h1 class="h3 mb-3 font-weight-normal">Sign-in</h1>
<?php
if (isset($data['errors']) && !empty($data['errors'])) { ?>
<div class="alert alert-danger" role="alert">
<?php foreach($data['errors'] as $error) {
    echo $error . '<br>';
} ?>
</div>
<?php } ?>
<label for="inputUsername" class="sr-only">Username</label>
<input type="text" name="username" value="<?php echo (isset($data['success']) && !$data['success'] && isset($_POST['username']) ? $_POST['username'] : '') ?>" id="inputUsername" class="form-control my-2" placeholder="Username" required autofocus>
<label for="inputPassword" class="sr-only">Password</label>
<input type="password" name="password" id="inputPassword" class="form-control my-2" placeholder="Password" required>
<button class="btn btn-lg btn-primary btn-block" type="submit">Sign-in</button>
</form>