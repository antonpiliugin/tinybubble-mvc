CREATE DATABASE IF NOT EXISTS `tinybubble` CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;

CREATE TABLE IF NOT EXISTS `tinybubble`.`users` (
    `user_id` INT AUTO_INCREMENT PRIMARY KEY,
    `username` VARCHAR(100) NOT NULL,
    `email` VARCHAR(255) NOT NULL,
    `password` VARCHAR(32) NOT NULL,
    `is_admin` TINYINT NOT NULL DEFAULT 0,
    CONSTRAINT `Unique_Person` UNIQUE (`username`, `email`)
) ENGINE=INNODB;

INSERT INTO `tinybubble`.`users` (`username`, `email`, `password`, `is_admin`) VALUES ('admin', 'admin@test.com', '202cb962ac59075b964b07152d234b70', 1);

CREATE TABLE IF NOT EXISTS `tinybubble`.`tasks` (
    `task_id` INT AUTO_INCREMENT PRIMARY KEY,
    `user_id` INT NOT NULL,
    `title` VARCHAR(255) NOT NULL,
    `status` TINYINT NOT NULL DEFAULT 0,
    `description` TEXT NOT NULL,
    `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    `admin_edit`TINYINT NOT NULL DEFAULT 0,
    FOREIGN KEY (`user_id`) REFERENCES `users`(`user_id`)
) ENGINE=INNODB;