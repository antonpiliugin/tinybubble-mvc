<?php

namespace TinyBubble\Models;

use TinyBubble\DB;

class Users {
    public function usernameExists($username) {
        return DB::getRow('SELECT `username` FROM `users` WHERE `username` = ?', $username);
    }
    public function emailExists($email) {
        return DB::getRow('SELECT `email` FROM `users` WHERE `email` = ?', $email);
    }
    public function create($username, $email, $password) {
        return DB::add('INSERT INTO `users` (`username`, `email`, `password`) VALUES(:username, :email, :password)', array(
            'username' => $username,
            'email' => $email,
            'password' => $password
        ));
    }
    public function login($username, $password) {
        return DB::getRow('SELECT `user_id`, `is_admin` FROM `users` WHERE `username` = :username AND `password` = :password', array(
            'username' => $username,
            'password' => $password
        ));
    }
}