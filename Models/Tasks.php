<?php

namespace TinyBubble\Models;

use TinyBubble\DB;

class Tasks {
    private static function getOffset($page = 1, $itemsPerPage = 3) {
        return (($page - 1) * $itemsPerPage) . ', ' . $itemsPerPage;
    }
    public function getPagesCount($itemsPerPage = 3) {
        $tasksCount = (int)DB::getValue('SELECT COUNT(*) FROM `tasks`');
        return (int)ceil($tasksCount / $itemsPerPage);
    }
    public function create($user_id, $title, $description) {
        return DB::add('INSERT INTO `tasks` (`user_id`, `title`, `description`) VALUES(:userid, :title, :description)', array(
            'userid' => $user_id,
            'title' => $title,
            'description' => $description
        ));
    }
    public function edit($task_id, $title, $description, $status, $admin_edit = 0) {
        return DB::set('UPDATE `tasks` SET `title` = :title, `description` = :description, `status` = :status, `admin_edit` = :adminedit WHERE `task_id` = :taskid LIMIT 1', array(
            'title' => $title,
            'description' => $description,
            'status' => $status,
            'adminedit' => $admin_edit,
            'taskid' => $task_id
        ));
    }
    public function displayTasks($page = 1, $itemsPerPage = 3, $order = '`created_at` DESC') {
        return DB::getAll('SELECT `t`.*, `u`.`email` AS `email`, `u`.`username` AS `username` FROM `tasks` AS `t` INNER JOIN `users` AS `u` ON `t`.`user_id` = `u`.`user_id` ORDER BY ' . $order . ' LIMIT ' . self::getOffset($page, $itemsPerPage));
    }
    public function getTask($id) {
        return DB::getRow('SELECT `t`.*, `u`.`email` AS `email`, `u`.`username` AS `username` FROM `tasks` AS `t` INNER JOIN `users` AS `u` ON `t`.`user_id` = `u`.`user_id` WHERE `task_id` = ? LIMIT 1', $id);
    }
}