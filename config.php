<?php
namespace TinyBubble;

define('CONTROLLERS_DIR', __DIR__ . '/Controllers/');
define('MODELS_DIR', __DIR__ . '/Models/');
define('VIEWS_DIR', __DIR__ . '/Views/');
define('DB_TYPE', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_PORT', '3306');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'qwerty');
define('DB_DATABASE', 'tinybubble');
?>