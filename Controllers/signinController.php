<?php
namespace TinyBubble\Controllers;

use TinyBubble\View;
use TinyBubble\Models\Users;

session_start();

class signinController {
    public function actionIndex() {
        if (isset($_SESSION['id'])) {
            header('Location: /tasks');
        } else {
            View::render('signin');
        }
    }

    public function actionAuth() {
        if (isset($_SESSION['id'])) {
            header('Location: /tasks');
        } else {
            $errors = array();
            if (!isset($_POST['username']) || empty($_POST['username'])) {
                $errors[] = 'Username must be set!';
            }
            if (!isset($_POST['password']) || empty($_POST['password'])) {
                $errors[] = 'Password must be set!';
            }
            if (empty($errors)) {
                $model = new Users();
                $user = $model->login($_POST['username'], md5($_POST['password']));
                if (!$user) {
                    $errors[] = 'Incorrect username or password!';
                } else {
                    $_SESSION['id'] = $user['user_id'];
                    $_SESSION['is_admin'] = $user['is_admin'];
                    header('Location: /tasks');
                    die();
                }
            }
            View::render('signin', array(
                'errors' => $errors,
                'success' => empty($errors)
            ));
        }
    }
}