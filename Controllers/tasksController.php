<?php
namespace TinyBubble\Controllers;

use TinyBubble\View;
use TinyBubble\Models\Tasks;

session_start();

class tasksController {
    public function actionIndex($param = null, $query = array()) {
        if (isset($_SESSION['id'])) {
            $model = new Tasks();
            $userPage = !isset($query['page']) ? 1 : filter_var($query['page'], FILTER_VALIDATE_INT);
            $itemsPerPage = 3;
            $totalPages = $model->getPagesCount($itemsPerPage);
            $page = !isset($query['page']) || (isset($query['page']) && $userPage > $totalPages || $userPage < 1) ? 1 : $userPage;
            $orderBy = '`created_at`';
            $orderDirection = 'DESC';
            $orderQueryArray = array();
            if (isset($query['orderBy']) && !empty($query['orderBy'])) {
                $orderQueryArray['orderBy'] = $query['orderBy'];
                switch ($query['orderBy']) {
                    case 'username':
                        $orderBy = '`username`';
                        break;
                    case 'email':
                        $orderBy = '`email`';
                        break;
                    case 'status':
                        $orderBy = '`status`';
                        break;
                    case 'created_at':
                        $orderBy = '`created_at`';
                        break;
                    default:
                        $orderBy = '`created_at`';
                }
            }
            if (isset($query['orderDirection']) && !empty($query['orderDirection'])) {
                $orderQueryArray['orderDirection'] = $query['orderDirection'];
                switch ($query['orderDirection']) {
                    case 'asc':
                        $orderDirection = 'ASC';
                        break;
                    case 'desc':
                        $orderDirection = 'DESC';
                        break;
                    default:
                        $orderDirection = 'DESC';
                }
            }
            $order = $orderBy . ' ' . $orderDirection;
            $orderQueryString = !empty($orderQueryArray) ? '&' . http_build_query($orderQueryArray) : '';
            $tasks = $model->displayTasks($page, $itemsPerPage, $order);
            View::render('tasks', array(
                'tasks' => $tasks,
                'itemsPerPage' => $itemsPerPage,
                'currentPage' => $page,
                'totalPages' => $totalPages,
                'orderQuery' => $orderQueryString,
                'message' => isset($_GET['message']) ? $_GET['message'] : ''
            ));
        } else {
            header('Location: /signin');
        }
    }
    public function actionCreate() {
        if (isset($_SESSION['id'])) {
            $errors = array();
            if (isset($_POST['title']) && empty($_POST['title'])) {
                $errors[] = 'Title must be set!';
            }
            if (isset($_POST['description']) && empty($_POST['description'])) {
                $errors[] = 'Description must be set!';
            }
            if (isset($_POST['title']) && isset($_POST['description']) && empty($errors)) {
                $model = new Tasks();
                if ($model->create($_SESSION['id'], $_POST['title'], $_POST['description'])) {
                    header('Location: /tasks?message=Task+has+been+created+successfully!');
                    die();
                } else {
                    $errors[] = 'Database error, please try again or contact support!';
                }
            }
            View::render('task', array(
                'errors' => $errors,
                'create' => true
            ));
        } else {
            header('Location: /signin');
        }
    }
    public function actionEdit($arg = 0) {
        if (isset($_SESSION['id'])) {
            $id = filter_var($arg, FILTER_VALIDATE_INT);
            if (!\is_int($id)) {
                header('Location: /tasks');
            } else {
                $model = new Tasks();
                $task = $model->getTask($id);
                if ($task) {
                    $errors = array();
                    if (isset($_POST['title']) && empty($_POST['title'])) {
                        $errors[] = 'Title must be set!';
                    }
                    if (isset($_POST['description']) && empty($_POST['description'])) {
                        $errors[] = 'Description must be set!';
                    }
                    if (isset($_POST['title']) && isset($_POST['description']) && empty($errors)) {
                        $model = new Tasks();
                        $admin_edit = (int)(($_SESSION['is_admin'] && $task['description'] !== $_POST['description']) || (int)$task['admin_edit']);
                        if ($model->edit($id, $_POST['title'], $_POST['description'], $_POST['status'], $admin_edit)) {
                            header('Location: /tasks?message=Task+has+been+updated+successfully!');
                            die();
                        } else {
                            $errors[] = 'Database error, please try again or contact support!';
                        }
                    } else {
                        header('Location: /tasks');
                    }
                } else {
                    header('Location: /tasks');
                }
            }
        } else {
            header('Location: /signin');
        }
    }
    public function actionShow($arg = 0) {
        if (isset($_SESSION['id'])) {
            $id = filter_var($arg, FILTER_VALIDATE_INT);
            if (!\is_int($id)) {
                header('Location: /tasks');
            } else {
                $model = new Tasks();
                $task = $model->getTask($id);
                if ($task) {
                    View::render('task', $task);
                } else {
                    header('Location: /tasks');
                }
            }
        } else {
            header('Location: /signin');
        }
    }
}