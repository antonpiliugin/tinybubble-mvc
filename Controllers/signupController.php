<?php
namespace TinyBubble\Controllers;

use TinyBubble\View;
use TinyBubble\Models\Users;

session_start();

class signupController {
    public function actionIndex() {
        if (isset($_SESSION['id'])) {
            header('Location: /tasks');
        } else {
            View::render('signup');
        }
    }

    public function actionCreate() {
        if (isset($_SESSION['id'])) {
            header('Location: /tasks');
        } else {
            $errors = array();
            if (!isset($_POST['username']) || empty($_POST['username'])) {
                $errors[] = 'Username must be set!';
            }
            if (!isset($_POST['email']) || empty($_POST['email'])) {
                $errors[] = 'E-mail must be set!';
            } else if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                $errors[] = 'E-mail adddress is not valid!';
            }
            if (!isset($_POST['password']) || empty($_POST['password'])) {
                $errors[] = 'Password must be set!';
            }
            if (!isset($_POST['password2']) || empty($_POST['password2'])) {
                $errors[] = 'Password confirmation must be set!';
            }
            if (isset($_POST['password']) && !empty($_POST['password']) && isset($_POST['password2']) && !empty($_POST['password2']) && $_POST['password'] !== $_POST['password2']) {
                $errors[] = 'Passwords are not equal!';
            }
            if (empty($errors)) {
                $model = new Users();
                if ($model->usernameExists($_POST['username'])) {
                    $errors[] = 'Username already exists!';
                }
                if ($model->emailExists($_POST['email'])) {
                    $errors[] = 'E-mail address already in use!';
                }
                if (empty($errors) && $model->create($_POST['username'], $_POST['email'], md5($_POST['password']))) {
                    //Send an email
                } else if (empty($errors)) {
                    $errors[] = 'Database error, please try again or contact support!';
                }
            }
            View::render('signup', array(
                'errors' => $errors,
                'success' => empty($errors)
            ));
        }
    }
}