<?php
namespace TinyBubble\Controllers;

use TinyBubble\View;
use TinyBubble\Models\Users;

session_start();

class signoutController {
    public function actionIndex() {
        if (isset($_SESSION['id'])) {
            session_unset();
            session_destroy(); 
        }
        header('Location: /');
    }
}