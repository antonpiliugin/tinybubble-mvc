<?php
namespace TinyBubble\Controllers;

use TinyBubble\View;
use TinyBubble\Models\Tasks;

session_start();

class homeController {
    public function actionIndex() {
        if (isset($_SESSION['id'])) {
            header('Location: /tasks');
        } else {
            View::render('home');
        }
    }
}