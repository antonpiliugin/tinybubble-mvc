<?php
namespace TinyBubble\Controllers;

use TinyBubble\View;
use TinyBubble\Models\Tasks;

class error404Controller {
    public function actionIndex() {
        View::render('404');
    }
}